import React, { useReducer, useEffect } from 'react';

import Mindful from '../mindful.png';
import './login.css';

//state type
type State = {
    username: string
    password:  string
    isButtonDisabled: boolean
    errorFeedback: string
    successFeedback: string
    isError: boolean
};

//Initial state for reducer 
const initialState:State = {
    username: '',
    password: '',
    isButtonDisabled: true,
    errorFeedback: '',
    successFeedback: '',
    isError: false
};

// Various actions to dispatch.
type Action = 
    { type: 'SET_USERNAME', payload: string }
  | { type: 'SET_PASSWORD', payload: string }
  | { type: 'SET_IS_BUTTON_DISABLED', payload: boolean }
  | { type: 'LOGIN_SUCCESS', payload: string }
  | { type: 'LOGIN_FAILED', payload: string }
  | { type: 'SET_IS_ERROR', payload: boolean };

// Reducer to track action and dispatch assign data to action
// NOTE: Instead of using reducer for setting userName and userPassword, we can also use useState hook
const reducer = (state: State, action: Action): State => {
    switch (action.type) {
        case 'SET_USERNAME': 
            return {
                ...state,
                username: action.payload
            };
        case 'SET_PASSWORD': 
            return {
                ...state,
                password: action.payload
            };
        case 'SET_IS_BUTTON_DISABLED': 
            return {
                ...state,
                isButtonDisabled: action.payload
            };
        case 'LOGIN_SUCCESS': 
            return {
                ...state,
                successFeedback: action.payload,
                isError: false
            };
        case 'LOGIN_FAILED': 
            return {
                ...state,
                errorFeedback: action.payload,
                isError: true
            };
        case 'SET_IS_ERROR': 
            return {
                ...state,
                isError: action.payload
            };
    }
}

const Login = () => {
    const   [state, dispatch] = useReducer(reducer, initialState);

    useEffect(() => {
        if(state.username.trim() && state.password.trim()) {
            dispatch({
                type: 'SET_IS_BUTTON_DISABLED',
                payload: false
            });
        }else {
            dispatch({
                type: 'SET_IS_BUTTON_DISABLED',
                payload: true
            });
        }     
    },[state.username, state.password]);

    //Function to submit form on button click.
    const handleLoginSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault()
        // console.log('State in handleLoginSubmit: ', state);
        if (state.username === 'abc@gmail.com' && state.password === 'password') {
            dispatch({
                type: 'LOGIN_SUCCESS',
                payload: `Logged in Successfully. Welcome to Mindful ${state.username}`
            });
            // alert(`LOGIN SUCCESSFUL. Welcome to Mindful ${state.username}`);
        }else {
            dispatch({
                type: 'LOGIN_FAILED',
                payload: 'Incorrect username or password'
            });
            // alert('LOGIN FAILED. PLEASE TRY AGAIN');
        }
    };

    const handleUsernameChange: React.ChangeEventHandler<HTMLInputElement> = (event) => {
        dispatch({
            type: 'SET_USERNAME',
            payload: event.target.value
        });
    };

    const handlePasswordChange: React.ChangeEventHandler<HTMLInputElement> = (event) => {
        dispatch({
            type: 'SET_PASSWORD',
            payload: event.target.value
        });
    }

    return (
        <div className='container-fluid login-container'>
            <div className='row login-image-row'>
                <img src={ Mindful } alt='Mindful-Login'/> 
            </div>
            <div className='row login-form-row'>  
                <div className='login-form-container'>
                    <div className='login-header-container'>
                        <p className='login-header'>Login</p>
                        <p className='login-sub-header'>Let’s help people around.</p>
                    </div>
                    <div className='login-form-body'>
                        <form onSubmit={ handleLoginSubmit } autoComplete="off">
                            <div className='form-group login-form-username-container'>
                                <input 
                                    name='username'
                                    type='text'
                                    id='username' 
                                    className='form-control login-form-username' 
                                    onChange={handleUsernameChange}
                                    placeholder='username'
                                />
                            </div>
                            <div className='form-group login-form-password-container'>       
                                <input 
                                    type='password' 
                                    className='form-control login-form-password' 
                                    name='password'
                                    onChange={(e) => handlePasswordChange(e)}
                                    placeholder='password'
                                />  
                                <p className={`${ state.isError ? 'login-error-feedback' : 'login-success-feedback'}`}>{ state.isError ?  state.errorFeedback : state.successFeedback }</p>      
                            </div>
                            <div className='login-button-container'>
                                <button type= 'submit' className= 'login-button' disabled={ state.isButtonDisabled } style={{'cursor': state.isButtonDisabled ? '' : 'pointer'}}>Login</button>
                            </div>       
                        </form>
                    </div>
                </div>
            </div>     
        </div>
    );
}

export default Login;