This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

###Description

A simple Login Form example using React and Typescript.

###Run Locally

1. Clone the Repo.
2. Navigate to the cloned folder on your local machine.
3. yarn install.
4. yarn run start

##Deployed link
This project has been deployed on Heroku at (https://login-type.herokuapp.com/)

###Login Credential

username- abc@gmail.com
password- password